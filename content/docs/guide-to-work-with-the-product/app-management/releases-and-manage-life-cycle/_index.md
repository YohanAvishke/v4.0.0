---
bookCollapseSection: true
weight: 2
---


# New Releases & Manage Life Cycle

<br><iframe width="560" height="315" src="https://www.youtube.com/embed/Lk493DLhl98" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<strong><h2>New App Releases</h2></strong>

Only enterprise apps can have multiple releases

<h2>Steps</h2>

<ul style="list-style-type:decimal;">
    <li>Find the enterprise application from the apps table of the publisher.</li>
    <li>Go to Add a new Release section of the application.</li>
    <li>Fill the form respectively (Don’t give the same apk for new release).</li>
    <li>After that go to previous release and change the state to ‘DEPRECATED’.</li>
    <li>Go to the newly added release and try to publish it.</li>
</ul>

<strong><h2>Manage Life Cycle</h2></strong>

<h2>Steps</h2>

<ul style="list-style-type:decimal;">
    <li>Find the published application from the table.</li>
    <li>Go to the published application and change the state to ‘‘DEPRECATED’’ -> “RETIRED”.</li>
    <li>Then the app will be removed from the store and the app details will not show on the publisher.</li>
</ul>
