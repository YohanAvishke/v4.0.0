---
bookCollapseSection: true
weight: 1
---


# App Publication & Add Reviews

<br><iframe width="560" height="315" src="https://www.youtube.com/embed/FnNi5ZMEdYU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<strong><h2>App Publish</h2></strong>

<h2>Steps</h2>

<ul style="list-style-type:decimal;">
    <li>Go to publisher portal .</li>
    <li>Then go to navigation bar and choose the application type from ‘Add New App’ drop down .</li>
    <li>Fill the forms respectively.</li>
    <li>After that find the application from the table and go to that application release.</li>
    <li>Change state to ‘IN-REVIEW’ -> ‘APPROVED’ -> ‘PUBLISHED’ </li>
    <li>Then go to the store section of the application.</li>
</ul>

<strong><h2>App Reviews</h2></strong>

<h2>Steps</h2>

<ul style="list-style-type:decimal;">
    <li>Find the published application from the the app store .</li>
    <li>Go to add the review section of the application and add a review.</li>
    <li>After that you can see the review details and the review rating on the bottom of the application page.</li>
</ul>
