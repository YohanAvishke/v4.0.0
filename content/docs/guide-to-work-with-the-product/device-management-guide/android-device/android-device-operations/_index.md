---
bookCollapseSection: true
weight: 1
---

# Add operation to an Android device

{{< hint info >}}
   <strong>Pre-requisites</strong>
   <br>
   <ul style="list-style-type:disc;">
       <li>Server is <a href="{{< param doclink >}}guide-to-work-with-the-product/download-and-start-the-server/">downloaded and started</a></li>
       <li>Logged into the server's <a href="{{< param doclink >}}guide-to-work-with-the-product/login-guide/">device mgt portal</a></li>
       <li><a href="{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/manage-an-enroll-device/#view-an-enrolled-device">View </a>the device that you have 
       enrolled.</li> 
   </ul>
   {{< /   hint >}}

1.Click on the operation that you need to apply to the device. In 
this tutorial, let's apply 
Ring device operation.

2.Then a pop up message will be displayed on the screen. And click on The button to confirm the operation.

<img src="../../../../image/60009.png" style="border:5px solid black" >

The following table lists out the operations that can be applied to the Android device

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th>Operation Type</th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Ring</strong></td>
            <td>Ability to Ring the device via Entgra IoT Server.
            </td>
        </tr>
        <tr>
            <td><strong>Device Lock</strong></td>
            <td>Ability to Lock the device via Entgra IoT Server.
                <br>When Hard lock enabled (OEM Mode Only) is checked, the device can be unlocked only through EMM. This is available only in COPE enrollment.
                <br>Optionally messege can be sent to the device via Entgra IoT Server while applying this operation.
                <img src="../../../../image/60010.png" style="border:5px solid black" >
            </td>
        </tr>
        <tr>
            <td><strong>Location</strong></td>
            <td>Ability to Request coordinates of device location via Entgra IoT Server.</td>
        </tr>
        <tr>
            <td><strong>Clear Password</strong></td>
            <td>Ability to Clear current password via Entgra IoT Server.
                <br>(This functionality is only working with profile owners from Android 7.0 API 24 onwards.).
            </td>
        </tr>
        <tr>
            <td><strong>Mute</strong></td>
            <td>Ability to Enable mute in the device via Entgra IoT Server.
            </td>
        </tr>
        <tr>
            <td><strong>Message</strong></td>
            <td>Ability to Send message operation via Entgra IoT Server. The title of the message can be defined seperately in the Title box. The message can be defined in the message box.
                <img src="../../../../image/60011.png" style="border:5px solid black" >
            </td>
        </tr>
        <tr>
            <td><strong>Change Lock-code</strong></td>
            <td>Ability to enable or disable current lock code via Entgra IoT Server.
                <br>(This functionality is only working with profile owners from Android 7.0 API 24 onwards.)
            </td>
        </tr>
        <tr>
            <td><strong>File Transfer</strong></td>
            <td>Ability to Transfer file via Entgra IoT Server.
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <center><strong>To device</strong></center>
                            </td>
                            <td>
                                <center><strong>From device</strong></center>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"> The supported file transferring protocols
                                <ul style="list-style-type:disc;">
                                    <li><strong>HTTP</strong> : HyperText Transfer Protocol
                                        <br>With this protocol each command is executed independently, without any knowledge of the commands that came before it. When HTTP is selected, HTTPS(HyperText Transfer Protocol Secure) is also supported.
                                    </li>
                                    <li><strong>FTP</strong> : File Transfer Protocol
                                        <br>FTP uses the Internet's TCP/IP protocols to enable data transfer.
                                    </li>
                                    <li><strong>SFTP</strong> : Secure File Transfer Protocol
                                        <br>It transfers files over a secure SSH secure data stream.</li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>File URL</strong>
                            </td>
                            <td>
                                <strong>URL to upload file from device</strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Location to save file in device</strong>
                            </td>
                            <td>
                                <strong>File location in the device</strong>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <center><strong>Authentication required</strong>
                                    <br> When Authentication required is checked, the password of the device will be verified before applying this operation to the device.</center>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="../../../../image/60012.png" style="border:5px solid black" >
                            </td>
                            <td>
                                <img src="../../../../image/60013.png" style="border:5px solid black">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td><strong>Enterprise Wipe</strong></td>
            <td>Ability to Remove enterprise applications via Entgra IoT Server.
            </td>
        </tr>
        <tr>
            <td><strong>Wipe Data</strong></td>
            <td>Ability to Factory reset the device via Entgra IoT Server.
                <br>Enter the Pin code of the device to complete this operation.
                <img src="../../../../image/60014.png" style="border:5px solid black "></td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>Available only in specific enrollment 
            types</strong></center>
            </td>
        </tr>
        <tr>
            <td><strong>Send app restriction</strong></td>
            <td>Ability to Send remote configurations to an app via Entgra IoT Server.
                <br>This configuration will be available only for COPE/COSU enrollment types.
                <br>The app to which the configuration is sent should be implemented in a way to retrieve the sent remote configurations.
                <br>Example:
                <ul>
                    <li>Application identifier : io.entgra.iot.restaurantapp</li>
                    <li>Application restriction payload : {"config":"testConfig"}</li>
                </ul>
                <img src="../../../../image/60015.png"  style="border:5px solid black "></td>
        </tr>
        <tr>
            <td><strong>Reboot</strong></td>
            <td>Ability to Reboot the device via Entgra IoT Server.
                <br>This configuration will be available only for COPE/COSU enrollment types.
            </td>
        </tr>
        <tr>
            <td><strong>Change LockTask</strong></td>
            <td>Ability to Change LockTask mode of KIOSK device via Entgra IoT Server.
                <br>This configuration will be available only for COSU enrollment type.</td>
        </tr>
        <tr>
            <td><strong>Upgrade Firmware</strong></td>
            <td>Ability to Upgrade Firmware via Entgra IoT Server.
                <br> Ability to Reboot the device via Entgra IoT Server.The time schedule for the firmware update and firmware update server address URL should be defined while applying this operation. When Instant Upgrade is checked the firmware will be upgraded instantly.
                <br>This configuration will be available only for COPE/COSU enrollment types.
                <img src="../../../../image/60016.png"  style="border:5px solid black">
            </td>
        </tr>
    </tbody>
</table>