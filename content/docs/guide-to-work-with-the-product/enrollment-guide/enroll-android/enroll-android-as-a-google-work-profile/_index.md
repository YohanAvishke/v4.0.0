---
bookCollapseSection: true
weight: 7
---

# Enroll Android as a Google work profile

Entgra IoT server is a [Google registered EMM vendor for work profiles](https://androidenterprisepartners.withgoogle.com/provider/#!/nO0FRKVachIRfVcd1gby). IoT server contains work profile creation ability and a certain integration with Google which allows Entgra to manage users Apps within the work profile using Google APIs. Before moving into enrollment of a device using this method, lets clarify the concepts in-depth.

## What is a work profile

A work profile is a containerized space in a personal mobile device that is reserved for cooperate use. Employees who brings in their devices to work, would prefer to keep their personal apps and data separated from cooperate apps and data. To achieve this, Android has a concept known as work profiles. A work profile creates a separate logical space in the device, reserved for cooperate apps and their data.

## Google work profile

IoT sever provides the ability to create a work profile and manage only the work profile and not the entire device. However, if the organization also need to manage the apps installed inside the work profile, a Google work profile is required. With a Google work profile, you as the customer, Entgra as the EMM vendor and Google as the application provider gets into a binding to provide app management inside the work profile.

### Advantages of Google work profile

<ul style="list-style-type:disc;">
    <li>Ability to install apps into the work profile.</li>
    <li>Automatically associate/create Google account for each work profile user which is used in the Playstore.</li>
    <li>Define what apps will be available/installed for each user.</li>
    <li>Install apps to work profile based on criteria such as battery condition, device idle status and network connectivity conditions.  </li>
    <li>Create a private cooperate app store on Google Playstore for organization.</li>
    <li>Define the layout of the Playstore on user's device.</li>
</ul>

### Auto generated Google account(Managed Google Play Accounts) vs exiting Google accounts(Google managed Accounts)

When the user creates a work profiles on the device as part of enrollment, Google work profile also allows to create a new Google account for each user and add it automatically to the work profile which can be used as the Google account for Playstore(This will be refered as "Managed Google Play Accounts" here on). If you already have Google accounts for all employees in the organization, it is also possible to use the same accounts to use during work profile creation(This will be refered as "Google managed Accounts" here on).

## Configurations and prerequisites for Google work profile

In order to enroll a device as a Google work profile, there are few prerequisites based on the Google account type(from previous topic) you wish to use. Prior to enrollment, lets look at the necessary configurations.

### Configurations and prerequisites for Managed Google Play Accounts

{{< hint info >}}
<strong>Pre-requisites</strong><br>
<ul style="list-style-type:disc;">
    <li>Request the Android for work token and server details from Entgra by writing to contact@entgra.io.</li>
</ul>

<ul style="list-style-type:disc;">
    While you are waiting for the token to arrive,
    <li>Make sure to review <a href="https://www.android.com/enterprise/terms/">managed Google play agreement</a> as you will have to agree to it during the process.</li>
    <li>Optionally have the Name, Email, phone number of following roles ready, if your organization has following roles,
    <ul style="list-style-type:circle;">
    <li>Data protection officer</li>
    <li>EU Representative</li>
    </ul>
    </li>
    <li>Create or have a personal Google account ready, that is not an account associated with G Suite or other managed domain accounts. Although this is a personal account, it's credentials must be shared with your organization. Therefore, it is recommended to create a new account for this purpose.</li>
    <br><b>Important Note</b> <br>
    Once the tokens are received, you will only have one hour to complete the process. Therefore, please have the above ready.
</ul>
{{< /   hint >}}

<b>If you have received tokens, </b> Lets proceed to next steps.
<ul style="list-style-type:decimal;">
<li>If the server is not started up, please start it.</li>
<li>Login to the Google account in a new private browsing window.</li>
<li>In the same private browser, Login to devicemgt console and go to PLATFORM CONFIGURATION -> Android Configurations.

<img src="../../../../image/2023.png" alt="drawing" style="width:75%; border:5pt solid black"/>

<li>Under "Android For Work Configurations" section, fill "Server details provided by EMM vendor" and "Token" with the value sent by Entgra and click "Begin" button. </li>

<li>You will be redirected to play.google.com. If you have not signed into the Google account, click sign in and sign in.</li>
<img src="../../../../image/2024.png" alt="drawing" style="width:75%; border:5pt solid black"/>

<li>Once loging in is completed, following screen will be presented and click "Get started" button.</li>
<img src="../../../../image/2026.png" alt="drawing" style="width:75%; border:5pt solid black"/><br/>

<li>Fill the details regarding Data protection officer and EU Representative if needed by your organization. This can be left blank as well to be filled later. Agree to managed Google play agreement and click "confirm" button.</li>
<img src="../../../../image/2027.png" alt="drawing" style="width:75%; border:5pt solid black"/><br/>

<li>From next screen click, "complete registration" button.</li>
<img src="../../../../image/2028.png" alt="drawing" style="width:75%; border:5pt solid black"/><br>

<li>Google will redirect to the EMM server and following screen will appear to indicate the successful completion of configuration.</li>
<img src="../../../../image/2029.png" alt="drawing" style="width:75%; border:5pt solid black"/>

<li>If we go to PLATFORM CONFIGURATION -> Android Configurations, now the ESA and Enterprise ID field values must be filled. For future reference, please copy both these values to a secure location and save. </li>
<img src="../../../../image/2030.png" alt="drawing" style="width:75%; border:5pt solid black"/>
</ul>

### Enroll a device

Download agent install the <a href="https://play.google.com/store/apps/details?id=io.entgra.iot.agent">Entgra EMM agent from playstore</a>
Enroll the device similar to how it is done with work profile.

Legacy enrollment is deprecated and is designed to take control of the device and data in a BYOD setting.

{{< hint info >}}
<strong>Pre-requisites</strong>
<br>
<ul style="list-style-type:disc;">
    <li>Server is <a href="{{< param doclink >}}guide-to-work-with-the-product/download-and-start-the-server/">downloaded and started</a></li>
        <li>Logged into the server's<a href="{{< param doclink >}}guide-to-work-with-the-product/login-guide/"> device mgt portal</a></li>
        <li>Optionally, basic <a href="{{< param doclink >}}key-concepts/#android ">concepts of Android device management</a> will be beneficial as well. </li>
         <li>Make sure to complete the 
    <a href="{{< param doclink >}}guide-to-work-with-the-product/enrollment-guide/enroll-android/enroll-android-as-a-google-work-profile/#enroll-android-as-a-google-work-profile">Enroll Android as a Google work profile</a> section before enrolling devices.
</ul>
{{< /   hint >}}

<iframe width="560" height="315" src="https://www.youtube.com/embed/lirSFp-t-38" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<h2>Steps</h2>

<ul style="list-style-type:decimal;">
    <li>In the devicemgt portal, Goto CONFIGURATION MANAGEMENT -> PLATFORM CONFIGURATIONS -> Android Configurations section and fill "SERVER_ADDRESS" with the gateway host and the polling interval(60seconds or more)
</li>
    <li>Download Entgra agent app from Google Playstore</li>
    <li>Go to add device section</li>
    <li>Under "Step 02 - Enroll the Android Agent." click enroll using QR</li>
    <li>From the dropdown, select Google_work_profile</li>
    <li>From the agent, select continue -> Enroll with QR code -> scan and scan the QR code and follow the on screen instructions to enroll</li>
</ul>

Note that any assigned apps will be automatically install.














